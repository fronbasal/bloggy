package main

import (
	"io/ioutil"
	"strings"

	"github.com/fronbasal/bloggy/helpers"

	"gopkg.in/russross/blackfriday.v2"

	"github.com/fronbasal/bloggy/structs"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

// Engine returns the gin engine
func Engine() *gin.Engine {

	r := gin.Default()
	r.Use(cors.Default())
	r.Use(func(c *gin.Context) {
		c.Header("X-Author", "github.com/fronbasal")
		c.Header("X-Project", "github.com/fronbasal/bloggy")
	})

	r.GET("/", func(c *gin.Context) {
		c.JSON(200, gin.H{"message": "Welcome to bloggy! A unique blogging API. Check the docs (github.com/fronbasal/bloggy) for details on how to use this API."})
	})

	api := r.Group("/api/v1")
	{
		api.GET("/list", func(c *gin.Context) {
			files, err := ioutil.ReadDir("posts")
			if err != nil {
				c.JSON(500, gin.H{"message": "An error occured while reading the posts", "error": err.Error()})
				return
			}
			var posts []structs.PostFile
			for _, f := range files {
				posts = append(posts, structs.PostFile{Timestamp: f.ModTime().Unix(), Name: strings.Replace(f.Name(), ".md", "", 1)})
			}
			c.JSON(200, gin.H{"count": len(files), "files": posts})
		})

		api.GET("/md/:post", func(c *gin.Context) {
			b := helpers.GetFile(c)
			c.String(200, string(b))
		})

		api.GET("/html/:post", func(c *gin.Context) {
			b := helpers.GetFile(c)
			c.Header("Content-Type", "text/html")
			c.String(200, string(blackfriday.Run(b)))
		})

		api.GET("/rawhtml/:post", func(c *gin.Context) {
			b := helpers.GetFile(c)
			c.String(200, string(blackfriday.Run(b)))
		})

	}

	return r
}

func main() {
	Engine().Run(":5000")
}
