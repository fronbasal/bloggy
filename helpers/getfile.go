package helpers

import (
	"io/ioutil"

	"github.com/gin-gonic/gin"
)

// GetFile returns the bytes of a file and matches it
func GetFile(c *gin.Context) []byte {
	b, err := ioutil.ReadFile("./posts/" + c.Param("post") + ".md")
	if err != nil {
		c.JSON(404, gin.H{"message": "The requested file does not exist (or a rare error occured)"})
	}
	return b
}
