# Bloggy

## About

Bloggy is a simple API for simple blogs.
It utilized MarkDown for ease of use and is supposed to be a minimal API (no auth, etc.).

To create a new post, write your markdown into a file and put it into your posts folder.

## Endpoints

To get a list of your posts, hit `/api/v1/list`.

To get a specific post in raw markdown, get `/api/v1/md/{post-name}`.

To receive a specific post in raw html (without browser rendering), request `/api/v1/rawhtml/{post-name}`.

To get a specific post *with* the Content-Type header (displays the rendered HTML in browser), request `/api/v1/html/{post-name}`.

## Install

### Docker

To deploy this app with docker, simply run following commands:

```bash
docker run -d -p 5000:5000 -v /path/to/posts:/go/src/app/posts fronbasal/bloggy
```

The license type is MIT.

Feel free to open an issue for any problems you might find!