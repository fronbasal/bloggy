package structs

// PostFile struct
type PostFile struct {
	Timestamp int64  `json:"timestamp"`
	Name      string `json:"name"`
}
